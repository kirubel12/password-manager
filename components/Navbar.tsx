"use client";
import React from "react";
import { Button } from "./ui/button";
import Link from "next/link";
import { auth } from "@/utils/firebaseClient";
import { useAuthState } from "react-firebase-hooks/auth";
import Image from "next/image";
import { signOut } from "firebase/auth";
import { usePathname, useRouter } from "next/navigation";
import { cn } from "@/lib/utils";

const Navbar = () => {
  const router = useRouter();
  const pathname = usePathname();
  const handleSignOut = () => {
    signOut(auth);
    router.push("/");
  };
  const [user, loading, error] = useAuthState(auth);

  return (
    <div className="navbar p-4 bg-base-100">
      <div className="navbar-start">
        <div className="dropdown">
          <div tabIndex={0} role="button" className="btn btn-ghost lg:hidden">
            <svg
              xmlns="http://www.w3.org/2000/svg"
              className="h-5 w-5"
              fill="none"
              viewBox="0 0 24 24"
              stroke="currentColor"
            >
              <path
                strokeLinecap="round"
                strokeLinejoin="round"
                strokeWidth="2"
                d="M4 6h16M4 12h8m-8 6h16"
              />
            </svg>
          </div>
          <ul
            tabIndex={0}
            className="menu menu-sm dropdown-content flex flex-col justify-between mt-3 z-[1] p-2 shadow bg-base-100 rounded-box w-52 space-y-4 "
          >
            {user && (
              <div className="space-y-4">
                <li>
                  <Link
                    href="/dashboard"
                    className={cn(
                      "hover:bg-zinc-950 hover:text-white hover:block hover:p-2 ",
                      pathname === "/dashboard"
                        ? "font-bold block bg-zinc-950 text-white p-2"
                        : ""
                    )}
                  >
                    Dashboard
                  </Link>
                </li>
                <li>
                  <Button variant="ghost" onClick={handleSignOut}>
                    Logout
                  </Button>
                </li>
              </div>
            )}
            {!user && (
              <div>
                <li>
                  <Link
                    href="/about-me"
                    className={cn(
                      " hover:text-white hover:block hover:p-2 ",
                      pathname === "/about-me"
                        ? "font-bold block bg-zinc-950 text-white p-2"
                        : ""
                    )}
                  >
                    About Me
                  </Link>
                </li>
              </div>
            )}
          </ul>
        </div>
        <Link href="/" className="text-xl font-semibold tracking-wider">
          PassGuard
        </Link>
      </div>
      <div className="navbar-center hidden lg:flex">
        <ul className="menu menu-horizontal px-1">
          {user && (
            <div className="flex gap-4">
              <li>
                <Link
                  href="/dashboard"
                  className={cn(
                    "hover:bg-zinc-950 hover:text-white hover:block hover:p-2 ",
                    pathname === "/dashboard"
                      ? "font-bold block bg-zinc-950 text-white p-2"
                      : ""
                  )}
                >
                  Dashboard
                </Link>
              </li>
              <Button variant="ghost" onClick={handleSignOut}>
                Logout
              </Button>
              <li>
                <Link
                  href="/about-me"
                  className={cn(
                    "hover:bg-zinc-600 hover:text-white  hover:p-2 ",
                    pathname === "/about-me"
                      ? "font-bold block bg-zinc-950 text-white p-2"
                      : ""
                  )}
                >
                  About Me
                </Link>
              </li>
            </div>
          )}
        </ul>
      </div>
      <div className="navbar-end">
        {user && (
          <Image
            src={user?.photoURL!}
            alt={user?.displayName!}
            className="rounded-full"
            width={60}
            height={60}
          />
        )}

        {!user && (
          <Link href="/login" className="">
            <Button className="px-6 py-2">Login</Button>
          </Link>
        )}
      </div>
    </div>
  );
};

export default Navbar;
