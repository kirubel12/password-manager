import { Button } from "@/components/ui/button";
import {
  Dialog,
  DialogClose,
  DialogContent,
  DialogDescription,
  DialogFooter,
  DialogHeader,
  DialogTitle,
  DialogTrigger,
} from "@/components/ui/dialog";
import { Input } from "@/components/ui/input";
import { Label } from "@/components/ui/label";
import { useState, useEffect } from "react";
import { generate } from "randomized-string";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, db } from "@/utils/firebaseClient";
import { addDoc, collection, serverTimestamp, doc } from "firebase/firestore";

import { toast } from "react-toastify";
import { useRouter } from "next/navigation";
const AddPassword = () => {
  const [user, loading, error] = useAuthState(auth);
  const [name, setName] = useState("");
  const [password, setPassword] = useState("");
  const [username, setUsername] = useState("");
  const [length, setLength] = useState(8);
  const router = useRouter();

  const generatePass = () => {
    const pass = generate({
      length: length,
      insertSymbol: true,
    });
    setPassword(pass);
  };

  const addMyPass = async () => {
    if (name.length == 0 || username.length == 0 || password.length == 0) {
      toast.error("Empty Field", {
        autoClose: 3000,
        position: "top-right",
        theme: "colored",
      });
    } else {
      const collectionRef = collection(db, "passwords");
      await addDoc(collectionRef, {
        timestamp: serverTimestamp(),
        userId: user?.uid,
        username: username,
        name: name,
        password: password,
      });

      toast.success("Added", {
        autoClose: 3000,
        position: "top-right",
        theme: "colored",
      });
      console.log(name, password, username, user?.uid);
    }
  };
  return (
    <div className="sm:m-4">
      <Dialog>
        <DialogTrigger asChild>
          <Button variant="default">Add Password</Button>
        </DialogTrigger>
        <DialogContent className="sm:max-w-[425px] gap-4">
          <DialogHeader>
            <DialogTitle>Create Magical Password</DialogTitle>
            <DialogDescription>
              make sure its the best password for you!
            </DialogDescription>
          </DialogHeader>
          <div className="grid gap-4 py-4">
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="name" className="text-right">
                Name
              </Label>
              <Input
                id="name"
                required
                value={name}
                onChange={(e) => setName(e.target.value)}
                defaultValue=""
                className="col-span-3"
              />
            </div>
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="name" className="text-right">
                Username/Email
              </Label>
              <Input
                id="username"
                value={username}
                required
                onChange={(e) => setUsername(e.target.value)}
                className="col-span-3"
              />
            </div>
            <div className="grid grid-cols-4 items-center gap-4">
              <Label htmlFor="username" className="text-right">
                Password
              </Label>
              <Input
                id="password"
                value={password}
                required
                onChange={(e) => setPassword(e.target.value)}
                type="text"
                defaultValue=""
                className="col-span-3"
              />
            </div>
          </div>
          <div>
            <div className="flex justify-between gap-4">
              <h2 className="mb-8">
                Password Length: <span className="font-bold">{length}</span>
              </h2>
              <Button size="sm" variant="outline" onClick={generatePass}>
                Generate
              </Button>
            </div>
            <input
              type="range"
              min="8"
              max="16"
              value={length}
              onChange={(e) => setLength(parseInt(e.target.value))}
              className="range "
            />
          </div>
          <DialogFooter className="mt-4">
            <DialogClose asChild>
              <Button
                disabled={
                  name.length == 0 ||
                  username.length == 0 ||
                  password.length == 0
                }
                onClick={addMyPass}
                type="submit"
                className="w-full"
              >
                Add password
              </Button>
            </DialogClose>
          </DialogFooter>
        </DialogContent>
      </Dialog>
    </div>
  );
};

export default AddPassword;
