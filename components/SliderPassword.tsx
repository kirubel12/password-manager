import { cn } from "@/lib/utils";
import { Slider } from "@/components/ui/slider";
import { useState } from "react";

type SliderProps = React.ComponentProps<typeof Slider>;
const SliderPassword = ({ className, ...props }: SliderProps) => {
  const [length, setLength] = useState<number>(8);
  return (
    <div>
      <Slider
        value={[length]}
        min={8}
        max={16}
        step={1}
        className={cn("w-[60%]", className)}
        {...props}
      />
    </div>
  );
};

export default SliderPassword;
