"use client";
import React from "react";
import { Button } from "./ui/button";

const Hero = () => {
  return (
    <div className="hero bg-white">
      <div className="hero-content text-center">
        <div className="max-w-md">
          <h1 className="text-5xl font-bold md:tracking-widest">
            Master Your Digital Security
          </h1>
          <p className="py-6">
            Safely store and manage your passwords with our robust and
            user-friendly password manager.
          </p>
          <Button className="md: px-6 py-2">Get Started</Button>
        </div>
      </div>
    </div>
  );
};

export default Hero;
