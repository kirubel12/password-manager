"use client";
import { FcGoogle } from "react-icons/fc";
import { signInWithPopup, GoogleAuthProvider } from "firebase/auth";
import { useRouter } from "next/navigation";
import { Button } from "@/components/ui/button";
import { auth } from "@/utils/firebaseClient";
import { useAuthState } from "react-firebase-hooks/auth";
import { useEffect } from "react";

const LoginPage = () => {
  const googleProvider = new GoogleAuthProvider();
  const router = useRouter();
  const [user] = useAuthState(auth);

  const GoogleLogin = async () => {
    try {
      const result = await signInWithPopup(auth, googleProvider);
      console.log(result.user);
      router.push("/dashboard");
    } catch (error) {
      console.log(error);
    }
    console.log("Google Login");
  };

  useEffect(() => {
    if (user) {
      router.push("/dashboard");
    }
  }, [user, router]);

  return (
    <div className="shadow-xl max-w-xl w-full mt-16 p-10 text-gray-700 rounded-lg">
      <h2 className="text-3xl font-medium">Join today</h2>
      <div className="py-4">
        <h3 className="py-4">Sign in with one of the providers</h3>
        <div className="flex flex-col gap-4">
          <Button className="flex align-middle gap-2 " onClick={GoogleLogin}>
            <FcGoogle className="text-2xl" />
            Sign in with Google
          </Button>
        </div>
      </div>
    </div>
  );
};

export default LoginPage;
