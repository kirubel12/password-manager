"use client";
import AddPassword from "@/components/AddPassword";

import { Button } from "@/components/ui/button";
import React, { Suspense, useEffect, useState } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
import { auth, db } from "@/utils/firebaseClient";
import Loading from "./loading";
import { FaEye } from "react-icons/fa6";

import {
  collection,
  deleteDoc,
  doc,
  onSnapshot,
  query,
  where,
} from "firebase/firestore";
import { useRouter } from "next/navigation";

type Data = {
  userId: string;
  name: string;
  username: string;
  password: string;
  date: Date;
  docId: string;
};

const Dashboard = () => {
  const [user, error, loading] = useAuthState(auth);
  const [myPassword, setMyPassword] = useState<
    {
      userId: string;
      name: string;
      username: string;
      password: string;
      date: Date;
      docId: string;
      id: string;
    }[]
  >([]);
  const [eyePsw, setEyePsw] = useState(false);
  const router = useRouter();

  const getData = async () => {
    if (loading) return;
    if (!user) return router.push("/login");

    const collectionRef = collection(db, "passwords");
    const q = query(collectionRef, where("userId", "==", user?.uid));
    const unsubscribe = onSnapshot(q, (snapshot) => {
      setMyPassword(
        snapshot.docs.map((doc) => ({ ...doc.data(), id: doc.id })) as {
          userId: string;
          name: string;
          username: string;
          password: string;
          date: Date;
          docId: string;
          id: string;
        }[]
      );
    });
    return unsubscribe;
  };

  const showCurrentPassword = (password: String) => {
    myPassword.map((pass) => {
      pass.password === password ? setEyePsw(!eyePsw) : setEyePsw(false);
    });
  };

  useEffect(() => {
    getData();
  });

  return (
    <Suspense fallback={<Loading />}>
      <header>
        <div className="mx-auto max-w-screen-xl px-4 pb-8 sm:px-6 sm:py-12 lg:px-8">
          <div className="sm:flex sm:items-center sm:justify-between">
            <div className="text-center sm:text-left">
              <h1 className="text-2xl font-bold text-gray-900 sm:text-3xl">
                Welcome Back, {user?.displayName}!
              </h1>

              <p className="mt-1.5 text-sm text-gray-500">
                Lets make a magic password!
              </p>
            </div>

            <div className="mt-4 flex flex-col gap-4 sm:mt-0 sm:flex-row sm:items-center">
              <div className="sm:w-full">
                <AddPassword />
              </div>
            </div>
          </div>
          <div className="mt-16">
            <div>
              <div className="overflow-x-auto">
                <table className="table">
                  <thead>
                    <tr>
                      <th>
                        <label>
                          <p>ID</p>
                        </label>
                      </th>
                      <th>User</th>
                      <th>Username</th>
                      <th>Name</th>
                      <th>Password</th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>
                    {myPassword.map((password) => {
                      return (
                        <tr key={password.docId}>
                          <th>
                            <label>
                              <p>{password.id}</p>
                            </label>
                          </th>
                          <td>
                            <h3>{user?.displayName}</h3>
                          </td>
                          <td>
                            <h4>{password.username}</h4>
                          </td>
                          <td>{password.name}</td>
                          <th className="flex ">
                            <input
                              id="psw"
                              type={eyePsw ? "text" : "password"}
                              value={password.password}
                            />
                          </th>
                          <th>
                            <FaEye
                              onClick={() => showCurrentPassword(password.id)}
                            />
                          </th>
                        </tr>
                      );
                    })}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </header>
    </Suspense>
  );
};

export default Dashboard;
