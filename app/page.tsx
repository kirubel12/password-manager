"use client";
import Hero from "@/components/Hero";
import { auth } from "@/utils/firebaseClient";
import { useRouter } from "next/navigation";
import { useEffect } from "react";
import { useAuthState } from "react-firebase-hooks/auth";
export default function Home() {
  const router = useRouter();
  const [user, loading, error] = useAuthState(auth);
  useEffect(() => {
    if (user) {
      router.push("/dashboard");
    }
  }, [user, router]);

  return (
    <main>
      <Hero />
    </main>
  );
}
